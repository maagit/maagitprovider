<?php
namespace Maagit\Maagitprovider\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Service
	class:				SortingService

	description:		Methods for sorting objects.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version
						2021-12-24	Urs Maag		Make compare function of usort with
													integer, not boolean

------------------------------------------------------------------------------------- */


class SortingService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * sort objects by properties and order
	 *
	 * @param	array				$objects			array of objects to sort
	 * @param	array				$sortKeys			array of sorting keys
	 * @return	int										the sorted objects
     */
	public function sort(array $objects, array $sortKeys)
	{
		usort($objects, $this->sortCompare($sortKeys));	
		return $objects;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * comparison function for using by sort function "usort"
	 *
	 * @param	array				$properties			properties to sort by:
	 *														- property name without "()"
	 *														- => sort order as value
	 *														- Example: ['getDatefrom' => 'asc']
	 * @return	int										the comparison result
     */
	protected function sortCompare(array $properties)
	{
		return function ($first, $second) use (&$properties)
		{
			foreach ($properties as $property => $order)
			{
				$dataType = gettype($first->$property());
				
				// sort data type string
				if ($dataType == 'string')
				{
					$diff = strcmp($first->$property(), $second->$property());
					if ($diff !== 0)
					{
						if ('asc' === strtolower($order))
						{
							return $diff;
						}
						return $diff * -1;
					}
				}

				// sort other data types than string (e.g. int, \DateTime, ...)
				if ($dataType != 'string')
				{
					if ('asc' === strtolower($order))
					{
						$diff = ($first->$property() > $second->$property()) ? 1 : -1;
					}
					else
					{
						$diff = ($first->$property() < $second->$property()) ? 1 : -1;	
					}
					return $diff;
				}
			}
			return 0;
		};
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}