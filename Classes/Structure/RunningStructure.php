<?php
namespace Maagit\Maagitprovider\Structure;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Structure
	class:				RunningStructure

	description:		Vaidate, if necessary structure is available, otherwise create it.

	created:			2022-11-12
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-11-12	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class RunningStructure
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Validate structure.
     *
     * @param	-
	 * @return void
     */
    public function validate()
    {
		// return, if init was already done
		if (file_exists(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('fileadmin/').'maagitprovider_init_done'))
		{
			return;
		}

		// render content to ask, if you want create the structure
		if (!isset($_POST['doStructure']))
		{
			echo file_get_contents(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:maagitprovider/Resources/Private/Structure/').'Structure.html');
			die;
		}

		// create structure and set init to "done"
		if (isset($_POST['doStructure']) && $_POST['doStructure'] == 'yes')
		{
			$this->createStructure();
			$this->initDone();
			return;
		}

		// set init to "done"
		if (isset($_POST['doStructure']) && $_POST['doStructure'] == 'no')
		{
			$this->initDone();
			return;
		}
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Create structure.
     *
     * @param	-
	 * @return	void
     */
	protected function createStructure()
	{
		// get root folder
		$storageRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\StorageRepository::class);
		$defaultStorage = $storageRepository->getDefaultStorage();
		$defaultStorage->setEvaluatePermissions(false);
		$root = $defaultStorage->getFolder('/');

		// create folders
		$template = $this->createSubfolder($root, 'template');
		if (isset($_POST['chkBackendlayout']) && $_POST['chkBackendlayout'] == 'Backendlayout')
		{
			$backendlayout = $this->createSubfolder($template, 'backendlayout');
		}
		if ((isset($_POST['chkImages']) && $_POST['chkImages'] == 'Images') || (isset($_POST['chkFavicons']) && $_POST['chkFavicons'] == 'Favicons'))
		{
			$images = $this->createSubfolder($template, 'images');
		}
		if (isset($_POST['chkFavicons']) && $_POST['chkFavicons'] == 'Favicons')
		{
			$favicon = $this->createSubfolder($images, 'favicon');
		}
		if (isset($_POST['chkCss']) && $_POST['chkCss'] == 'Css')
		{
			$css = $this->createSubfolder($template, 'css');
		}
		if ((isset($_POST['chkScss']) && $_POST['chkScss'] == 'Scss') || (isset($_POST['chkTheme']) && $_POST['chkTheme'] == 'Theme'))
		{
			$scss = $this->createSubfolder($template, 'scss');
		}
		if (isset($_POST['chkJs']) && $_POST['chkJs'] == 'Js')
		{
			$js = $this->createSubfolder($template, 'js');
		}
		if (isset($_POST['chkTemplates']) && $_POST['chkTemplates'] == 'Templates')
		{
			$templates = $this->createSubfolder($template, 'templates');
			$partials = $this->createSubfolder($template, 'partials');
		}
		if (isset($_POST['chkFluidTemplates']) && $_POST['chkFluidTemplates'] == 'FluidTemplates')
		{
			$extensions = $this->createSubfolder($template, 'extensions');
			$fluidStyledContent = $this->createSubfolder($extensions, 'fluid_styled_content');
			$fluidStyledContentLayouts = $this->createSubfolder($fluidStyledContent, 'Layouts');
			$fluidStyledContentTemplates = $this->createSubfolder($fluidStyledContent, 'Templates');
			$fluidStyledContentPartials = $this->createSubfolder($fluidStyledContent, 'Partials');
			$fluidStyledContentPartialsCustom = $this->createSubfolder($fluidStyledContentPartials, 'Custom');
			$fluidStyledContentPartialsHeader = $this->createSubfolder($fluidStyledContentPartials, 'Header');
			$fluidStyledContentPartialsMedia = $this->createSubfolder($fluidStyledContentPartials, 'Media');
			$fluidStyledContentPartialsMediaCustom = $this->createSubfolder($fluidStyledContentPartialsMedia, 'Custom');
			$fluidStyledContentPartialsMediaRendering = $this->createSubfolder($fluidStyledContentPartialsMedia, 'Rendering');
			$fluidStyledContentPartialsMediaType = $this->createSubfolder($fluidStyledContentPartialsMedia, 'Type');
			$fluidStyledContentPartialsMediaTypeCustom = $this->createSubfolder($fluidStyledContentPartialsMediaType, 'Custom');
		}

		// copy files
		$resourcesPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:maagitprovider/Resources/Public/Fileadmin/');
		if (isset($_POST['chkBackendlayout']) && $_POST['chkBackendlayout'] == 'Backendlayout') {$this->copyFiles($resourcesPath.'backendlayout', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($backendlayout->getPublicUrl(), 1)));}
		if (isset($_POST['chkImages']) && $_POST['chkImages'] == 'Images') {$this->copyFiles($resourcesPath.'images', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($images->getPublicUrl(), 1)));}
		if (isset($_POST['chkFavicons']) && $_POST['chkFavicons'] == 'Favicons') {$this->copyFiles($resourcesPath.'images/favicon', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($favicon->getPublicUrl(), 1)));}
		if (isset($_POST['chkCss']) && $_POST['chkCss'] == 'Css') {$this->copyFiles($resourcesPath.'css', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($css->getPublicUrl(), 1)));}
		if (isset($_POST['chkScss']) && $_POST['chkScss'] == 'Scss') {$this->copyFiles($resourcesPath.'scss', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($scss->getPublicUrl(), 1)));}
		if (isset($_POST['chkJs']) && $_POST['chkJs'] == 'Js') {$this->copyFiles($resourcesPath.'js', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($js->getPublicUrl(), 1)));
		if (isset($_POST['chkTemplates']) && $_POST['chkTemplates'] == 'Templates')
		{
			$this->copyFiles($resourcesPath.'templates', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($templates->getPublicUrl(), 1)));}
			$this->copyFiles($resourcesPath.'partials', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($partials->getPublicUrl(), 1)));
		}
		if (isset($_POST['chkFluidTemplates']) && $_POST['chkFluidTemplates'] == 'FluidTemplates')
		{
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Layouts', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentLayouts->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Templates', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentTemplates->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Custom', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsCustom->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Header', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsHeader->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Media', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsMedia->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Media/Custom', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsMediaCustom->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Media/Rendering', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsMediaRendering->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Media/Type', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsMediaType->getPublicUrl(), 1)));
			$this->copyFiles($resourcesPath.'extensions/fluid_styled_content/Partials/Media/Type/Custom', \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($fluidStyledContentPartialsMediaTypeCustom->getPublicUrl(), 1)));
		}

		// write file contents for theme.css
		if (isset($_POST['chkTheme']) && $_POST['chkTheme'] == 'Theme')
		{
			$resourcesPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('EXT:maagitprovider/Resources/Public/');
			$content = '';
			$content .= '@import "'.$resourcesPath.'StyleSheet/Bootstrap/functions";'."\n";
			$content .= '@import "'.$resourcesPath.'StyleSheet/Bootstrap/variables";'."\n";
			$content .= '@import "'.$resourcesPath.'StyleSheet/Bootstrap/mixins";'."\n";
			file_put_contents(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(substr($scss->getPublicUrl(), 1)).'theme.scss', $content);
		}
	}
	
	/**
     * Check/create "fileadmin/template" folder structure.
     *
     * @param	\TYPO3\CMS\Core\Resource\Folder				$folder					the folder to create the subfolder in
	 * @param	string										$subfolder				the subfolder name
	 * @return	TYPO3\CMS\Core\Resource\Folder										the created folder
     */
	protected function createSubfolder(\TYPO3\CMS\Core\Resource\Folder $folder, string $subfolder)
	{
		try {$subfolder = $folder->createFolder($subfolder);} catch(\Exception $ex) {$subfolder = $folder->getSubfolder($subfolder);}
		return $subfolder;
	}

	/**
     * Copy files
     *
     * @param	string										$source					the source path
	 * @param	string										$destination			the destination path
	 * @return	void
     */
	protected function copyFiles(string $source, string $destination)
	{
		$dir = opendir($source); 
		if ($dir !== false)
		{
			while ($file = readdir($dir))
			{ 
				if (($file != '.') && ($file != '..'))
				{
					if (!is_dir($source.'/'.$file))
					{
						copy($source.'/'.$file, $destination.$file); 
					}
				}
			}
			closedir($dir);	
		}
	}

	/**
     * Write "initialization done" file to fileadmin root
     *
     * @param	-
	 * @return	void
     */
	protected function initDone()
	{
		$file = fopen(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('fileadmin/').'maagitprovider_init_done', 'w');
		fclose($file);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
?>