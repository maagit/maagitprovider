<?php
	return [
	    'web_formdata' => [
	        'parent' => 'web',
	        'position' => ['after' => 'FormFormbuilder'],
	        'access' => 'user',
	        'workspaces' => '*',
	        'iconIdentifier' => 'module-formdata',
	        'path' => '/module/web/formdata',
	        'labels' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang_mod.xlf',
			'extensionName' => 'Maagitprovider',
	        'controllerActions' => [
	            Maagit\Maagitprovider\Controller\FormdataController::class => [
	                'formdata',
	                'delete'
				]
			]
		]
	];
?>