<?php
	return [
		// required import configurations of other extensions,
	    // in case a module imports from another package
		'dependencies' => ['form'],
		'imports' => [
			// recursive definiton, all *.js files in this folder are import-mapped
			// trailing slash is required per importmap-specification
			'@maagit/maagitprovider/' => 'EXT:maagitprovider/Resources/Public/JavaScript/',
		],
	];