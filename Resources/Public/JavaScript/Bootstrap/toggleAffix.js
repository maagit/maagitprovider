var toggleAffix = function(affixElement, scrollElement, fullHeight) {
	if (scrollElement.scrollTop() > 0) {
		affixElement.css('min-height', '0');
		if (typeof affixSmall === 'function') { 
		    affixSmall(affixElement);
		}
	} else {
		affixElement.css('min-height', fullHeight);
		if (typeof affixNormal === 'function') { 
		    affixNormal(affixElement);
		}
	}
};

$( document ).ready(function() {
	$('[data-toggle="affix"]').each(function() {
		var ele = $(this);
		var fullHeight = ele.outerHeight();
		ele.css('min-height', fullHeight);
		ele.addClass('affix');
		$(window).on('scroll resize', function() {
			toggleAffix(ele, $(this), fullHeight);
		});
		toggleAffix(ele, $(window), fullHeight);
		if (typeof affixInit === 'function') { 
		    affixInit(ele);
		}
	});
});