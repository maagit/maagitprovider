<?php
namespace Maagit\Maagitprovider\FormValidators;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Form
	class:				WordScannerValidator

	description:		Scan content of defined words and links.

	created:			2022-08-14
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-08-14	Urs Maag		Initial version
						2022-10-06	Urs Maag		Remove constructor and change
													the $supportedOptions to array
													values

------------------------------------------------------------------------------------- */


class WordScannerValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     *
     * @var string
     */
	protected $words = '';

	/**
     *
     * @var boolean
     */
	protected $links = false;

	/**
     *
     * @var array
     */
	protected $supportedOptions = [
		'words' => ['', 'Words, which are forbidden', 'string'],
		'links' => [false, 'Allow links?', 'boolean']
	];


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Validate form value
     *
	 * @param	mixed			$value				the value to validate
	 * @return	boolean								the validation result
	 */
	protected function isValid(mixed $value): void
	{
		$this->words = $this->options['words']??'';
		$this->links = $this->options['links']??false;
		if ($this->hasLinks($value) || $this->hasWords($value))
		{
			$this->addError('validation failed', 1660495371);
		}
	}
	
	/**
     * Check, if there are links in given value
     *
     * @param	string					$value				the value to check
     * @return	boolean
     */
	protected function hasLinks(string $value)
	{
		if ($this->links)
		{
			$pattern = '~[a-z]+://\S+~';
			$num_found = preg_match_all($pattern, $value, $out);
			if ($num_found > 0) {return true;}
		}
		return false;
	}
	
	/**
     * Check, if there are given words in given value
     *
     * @param	string					$value			the value to check
     * @return	boolean
     */
	protected function hasWords(string $value)
	{
		$words = explode(',', $this->words);
		$found = false;
		foreach ($words as $word)
		{
			if (!empty(trim($word)))
			{
				if (strpos(strtolower($value), strtolower(trim($word))) !== FALSE)
				{
					$found = true;
					break;
				}
			}
		}
		return $found;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}