<?php
namespace Maagit\Maagitprovider\FormElements;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Form
	class:				RecordListOptions

	description:		Get record list entries from given pid and render a list form
						element.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\FrontendRestrictionContainer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\GenericFormElement;
use TYPO3\CMS\Frontend\Category\Collection\CategoryCollection;

class RecordlistOptions extends GenericFormElement
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * set property -> options for recordlist
     *
     * @param	string				$key		the key of form element
	 * @param	mixed				$value		the value for this element
     * @return	void
     */
	public function setProperty(string $key, $value)
    {
		if ($key === 'recordlistPid') {
            $this->setProperty('options', $this->getOptions($value));
            return;
        }
        parent::setProperty($key, $value);
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * get options for recordlist
     *
     * @param	int					$pid		the pid, which contains the record options
     * @return	array							the options
     */
	protected function getOptions(int $pid) : array
    {
        $options = array();
        foreach ($this->getRecordlistEntriesForPid($pid) as $entry)
		{
            $options[$entry['fieldValue']] = $entry['fieldText'];
        }
        return $options;
    }

    /**
     * select entries from database
     *
     * @param	int					$pid		the pid, which contains the record options
     * @return	array							the selected rows
     */
    protected function getRecordlistEntriesForPid(int $pid) : array
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable('tx_maagitprovider_domain_model_recordlist');
        $queryBuilder->setRestrictions(
            GeneralUtility::makeInstance(FrontendRestrictionContainer::class)
        );
        return $queryBuilder
            ->select('*')
            ->from('tx_maagitprovider_domain_model_recordlist')
            ->where(
                $queryBuilder->expr()->eq(
                    'pid',
                    $queryBuilder->createNamedParameter($pid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
                )
            )
			->orderBy('sorting')
            ->executeQuery()
            ->fetchAllAssociative();
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}