<?php
namespace Maagit\Maagitprovider\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Controller
	class:				ContentController

	description:		Get typoscript code from content element and parse it.

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2021-12-23	Urs Maag		PHP 8, remove optional parameter
													"script" on method "scriptParser"
						2024-10-17	Urs Maag		Remove protected methods for
													parsing typoscript (newly make
													it with TyposcriptStringFactory)

------------------------------------------------------------------------------------- */


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Frontend\Configuration\TypoScript\ConditionMatching\ConditionMatcher;
use TYPO3\CMS\Core\EventDispatcher\NoopEventDispatcher;
use TYPO3\CMS\Core\TypoScript\AST\AstBuilder;
use TYPO3\CMS\Core\TypoScript\TypoScriptStringFactory;

class ContentController extends ActionController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     *
     * @var CONSTANT: Recursive level
     */
	const RECURSIVE_LEVEL = 10;

    /**
     *
     * @var ConditionMatcher
     */
    protected $matchCondition;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Action for this controller.
	 * Parse typoscript and render content.
     *
     * @return void
     */
    public function indexAction()
    {
        //TypoScript configuration given from tt_content record
		// @extensionScannerIgnoreLine
		$contentObject = $this->request->getAttribute('currentContentObject');
        // @extensionScannerIgnoreLine
		$configuration = $contentObject->data['bodytext'];

		$typoScriptStringFactory = GeneralUtility::makeInstance(TypoScriptStringFactory::class);
		$typoScriptTree = $typoScriptStringFactory->parseFromString($configuration, new AstBuilder(new NoopEventDispatcher()));
		$setup = $typoScriptTree->toArray();
		$result = $contentObject->cObjGet($setup, 'typoscript_code_proc.');

		return $this->responseFactory->createResponse()
			->withAddedHeader('Content-Type', 'text/html; charset=utf-8')
			->withBody($this->streamFactory->createStream($result));
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
?>