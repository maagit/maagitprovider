mod {
	web_layout {
		BackendLayouts {
			10 {
    			title = Startseite
    			icon = 
    			config {
	      	  		backend_layout {
	        			colCount = 2
		        		rowCount = 3
						rows {
							1 {
								columns {
									1 {
										name = Slider
										colspan = 2
										colPos = 10
									}
								}
							}
							2 {
								columns {
									1 {
										name = Inhalt
										colspan = 2
										colPos = 50
									}
								}
							}
							3 {
								columns {
									1 {
										name = Fusszeile - Copyright
										colPos = 94
									}
									2 {
										name = Fusszeile - Additional Links
										colPos = 95
									}
								}
							}
						}
					}
				}
			}
			20 {
    			title = 1-Spaltig
    			icon = 
    			config {
	      	  		backend_layout {
	        			colCount = 2
		        		rowCount = 2
						rows {
							1 {
								columns {
									1 {
										name = Inhalt
										colspan = 2
										colPos = 50
									}
								}
							}
							2 {
								columns {
									1 {
										name = Fusszeile - Copyright
										colPos = 94
									}
									2 {
										name = Fusszeile - Additional Links
										colPos = 95
									}
								}
							}
						}
					}
				}
			}
			90 {
    			title = Content
    			icon = 
    			config {
	      	  		backend_layout {
	        			colCount = 1
		        		rowCount = 1
						rows {
							1 {
								columns {
									1 {
										name = Inhalt
										colPos = 50
									}
								}
							}
						}
					}
				}
			}
		}
	}
}