<?php
namespace Maagit\Maagitprovider\Scss;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			scss
	class:				Autoprefixter

	description:		Formatter for generated scss files.
						Uses the csscrush library.

	created:			2020-08-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-03	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Autoprefixer extends ScssPhp\ScssPhp\Formatter
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
    public function __construct()
    {
        $this->indentLevel = 0;
        $this->indentChar = '  ';
        $this->break = '';
        $this->open = '{';
        $this->close = '}';
        $this->tagSeparator = ',';
        $this->assignSeparator = ':';
        $this->keepSemicolons = false;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	public function blockLines(\ScssPhp\ScssPhp\Formatter\OutputBlockOutputBlock $block)
	{
		$inner = $this->indentStr();
		$glue = $this->break . $inner;

		foreach ($block->lines as $index => $line)
		{
			require_once __DIR__ . '/../../Resources/Private/Libraries/csscrush/CssCrush.php';
			$line = csscrush_string('.crushwrapper {'.$line.'}',['minify' => true,'boilerplate' => false, 'formatter' => 'single-line', 'versioning' => false]);
			$line = str_replace(['.crushwrapper {', '}'], [''], $line);
			$line = preg_replace( "/\r|\n/", "", $line);
			if (0 === strpos($line, '/*') && $line[2] !== '!')
			{
				unset($block->lines[$index]);
			}
			elseif (0 === strpos($line, '/*!'))
			{
				$block->lines[$index] = '/*' . substr($line, 3);
			}
			else
			{
				$block->lines[$index] = $line;
			}
		}
		$this->write($inner.implode($glue, $block->lines));
		if (!empty($block->children))
		{
			$this->write($this->break);
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}