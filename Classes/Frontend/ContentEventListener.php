<?php
namespace Maagit\Maagitprovider\Frontend;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Frontend
	class:				ContentEventListener

	description:		Analyze and replace content.

	created:			2022-11-22
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-11-15	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class ContentEventListener
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Analyze and replace content.
     *
     * @return void
     */
    public function __invoke(\TYPO3\CMS\Frontend\Event\AfterCacheableContentIsGeneratedEvent $event)
    {
		// @extensionScannerIgnoreLine
		$event->getController()->content = $this->replaceFontawesomeTags($event->getController()->content);
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Analyze and replace content.
	 * Convert text fragments for fontawesome as follows
	 * - '#fa-car fa-solid#' to '<i class="fa fa-car fa-solid"></i>'
     *
     * @params	string					$content		the content
	 * @return	string									the replaced content
     */
	protected function replaceFontawesomeTags(string $content)
	{
		preg_match_all('/#fa-(.*?)#/', $content, $tags);
		foreach ($tags[0] as $tag)
		{
			$tag = str_replace('#', '', $tag);
			$content = str_replace('#'.$tag.'#', '<i class="fa '.$tag.'" aria-hidden="true"></i>', $content);
		}
		return $content;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}
?>