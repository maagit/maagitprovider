<?php
$EM_CONF[$_EXTKEY] = [
	'title' => 'MaagIT Provider',
	'description' => 'Base settings and extended functionality for using websites provided by TYPO3 CMS.',
	'category' => 'plugin',
	'author' => 'Urs Maag',
	'author_email' => 'info@maagit.ch',
	'author_company' => 'maagIT',
	'state' => 'stable',
	'createDirs' => '',
	'clearCacheOnLoad' => 1,
	'version' => '13.4.5',
	'constraints' => [
		'depends' => [
			'typo3' => '13.4.5-13.99.99'
		],
		'conflicts' => [

		],
		'suggests' => [

		]
	]
];
