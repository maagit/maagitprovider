<?php
call_user_func(function () {
	// WORK AROUND OF BUG: #96135 (IRRE: Hide Record switch not work)
	$GLOBALS['TCA']['sys_file_reference']['columns']['hidden']['config']['renderType'] = 'check';
	$GLOBALS['TCA']['sys_file_reference']['columns']['hidden']['config']['items'][0]['invertStateDisplay'] = false;


	// ICONS: Add type icon classes
	$GLOBALS['TCA']['tt_content']['ctrl']['typeicon_classes']['maagitprovider_content'] = 'extensions-maagitprovider_content';


	// TS CONTENT: TCA definition
	$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['showitem'] = '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.general;general,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header,bodytext,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.appearanceLinks;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ';

	// TS CONTENT: Activate t3editor for tt_content type maagitprovider_content if this type exists and t3editor is loaded
    if (array_key_exists('maagitprovider_content', $GLOBALS['TCA']['tt_content']['types']))
	{
		if (is_array($GLOBALS['TCA']['tt_content']['types']['maagitprovider_content'])) {
			if (array_key_exists('columnsOverrides', $GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']))
			{
				if (!is_array($GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides'])) {
					$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides'] = [];
				}
				if (array_key_exists('bodytext', $GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']))
				{
					if (!is_array($GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext'])) {
						$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext'] = [];
					}
					if (array_key_exists('config', $GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']))
					{
						if (!is_array($GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config'])) {
							$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config'] = [];
						}
					}
				}
			}
			$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config']['type'] = 'text';
			$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config']['enableRichtext'] = false;
			if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('t3editor'))	{
				$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config']['renderType'] = 't3editor';
			}
			$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config']['wrap'] = 'off';
			$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['config']['format'] = 'typoscript';
			$GLOBALS['TCA']['tt_content']['types']['maagitprovider_content']['columnsOverrides']['bodytext']['label'] = 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:bodytext';
		}
	}

	// TS CONTENT: Register the plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'Maagitprovider',
        'Content',
        'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:plugins.title',
        'extensions-maagitprovider_content'
    );
});
?>