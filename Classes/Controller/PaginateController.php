<?php
namespace Maagit\Maagitprovider\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Widget
	class:				PaginateController

	description:		Controller for the paginate widget. Works with arrays - the
						original backend paginate widget does not work with arrays.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PaginateController extends AbstractWidgetController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var array
     */
    protected $configuration = [
		'itemsPerPage' => 10,
		'insertAbove' => false,
		'insertBelow' => true,
		'recordsLabel' => ''
	];

    /**
     * @var mixed
     */
    protected $objects;

    /**
     * @var int
     */
    protected $currentPage = 1;

    /**
     * @var int
     */
    protected $numberOfPages = 1;

    /**
     * @var int
     */
    protected $offset = 0;

    /**
     * @var int
     */
    protected $itemsPerPage = 0;

    /**
     * @var int
     */
    protected $numberOfObjects = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * Initializes necessary variables for all actions.
	 *
	 * @param	-
	 * @return	void
	 */
	public function initializeAction()
	{
		$this->objects = $this->widgetConfiguration['objects'];
		\TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule($this->configuration, $this->widgetConfiguration['configuration'], false);
		$this->numberOfObjects = count($this->objects);
		$itemsPerPage = (int)$this->configuration['itemsPerPage'];
		$this->numberOfPages = $itemsPerPage > 0 ? (int)ceil($this->numberOfObjects / $itemsPerPage) : 0;
    }

	/**
	 * Render the paginate widget
	 *
	 * @param	int				$currentPage		the current page
	 * @return	void
	 */
    public function indexAction($currentPage = 1)
    {
		// set current page
		$this->currentPage = max((int)$currentPage, 1);
		$this->currentPage = min($this->numberOfPages, $this->currentPage);

		if ($this->currentPage > $this->numberOfPages)
		{
			// set $modifiedObjects to NULL if the page does not exist
			// (happens when numberOfPages is zero, not having any items)
			$modifiedObjects = null;
		}
		else
		{
			// modify objects
			$this->itemsPerPage = (int)$this->configuration['itemsPerPage'];
			$this->offset = 0;
			if ($this->objects instanceof \TYPO3\CMS\Extbase\Persistence\QueryResultInterface)
			{
				$this->offset = (int)$this->objects->getQuery()->getOffset();
			}
			if ($this->currentPage > 1)
			{
				$this->offset = $this->offset + ((int)($this->itemsPerPage * ($this->currentPage - 1)));
			}
			$modifiedObjects = $this->prepareObjectsSlice($this->itemsPerPage, $this->offset);
		}
		$this->view->assign('contentArguments', [
			$this->widgetConfiguration['as'] => $modifiedObjects
		]);
		$this->view->assign('configuration', $this->configuration);
		$this->view->assign('pagination', $this->buildPagination());
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
	 * Returns an array with the keys "current", "numberOfPages", "nextPage", "previousPage", "startRecord", "endRecord"
	 *
	 * @param	-
	 * @return	array								the pagination configuration
	 */
	protected function buildPagination()
    {
		$endRecord = $this->offset + $this->itemsPerPage;
		if ($endRecord > $this->numberOfObjects)
		{
			$endRecord = $this->numberOfObjects;
		}
		$pagination = [
			'current' => $this->currentPage,
			'numberOfPages' => $this->numberOfPages,
			'hasLessPages' => $this->currentPage > 1,
			'hasMorePages' => $this->currentPage < $this->numberOfPages,
			'startRecord' => $this->offset + 1,
			'endRecord' => $endRecord
		];
		if ($this->currentPage < $this->numberOfPages)
		{
			$pagination['nextPage'] = $this->currentPage + 1;
		}
		if ($this->currentPage > 1)
		{
			$pagination['previousPage'] = $this->currentPage - 1;
		}
		return $pagination;
	}

	/**
	 * Returns an array with the keys "current", "numberOfPages", "nextPage", "previousPage", "startRecord", "endRecord"
	 *
	 * @param	int					$itemsPerPage		count of items per page
	 * @param	int					$offset				the offset
	 * @return	array									the modified objects
	 */
	protected function prepareObjectsSlice($itemsPerPage, $offset)
	{
		if ($this->objects instanceof \TYPO3\CMS\Extbase\Persistence\QueryResultInterface)
		{
			$currentRange = $offset + $itemsPerPage;
			$endOfRange = min($currentRange, count($this->objects));
			$query = $this->objects->getQuery();
			$query->setLimit($itemsPerPage);
			if ($offset > 0)
			{
				$query->setOffset($offset);
				if ($currentRange > $endOfRange)
				{
					$newLimit = $endOfRange - $offset;
					$query->setLimit($newLimit);
				}
			}
			$modifiedObjects = $query->executeQuery();
			return $modifiedObjects;
		}
		if ($this->objects instanceof \TYPO3\CMS\Extbase\Persistence\ObjectStorage)
		{
			$modifiedObjects = [];
			$objectArray = $this->objects->toArray();
			$endOfRange = min($offset + $itemsPerPage, count($objectArray));
			for ($i = $offset; $i < $endOfRange; $i++)
			{
				$modifiedObjects[] = $objectArray[$i];
			}
			return $modifiedObjects;
		}
		if (is_array($this->objects))
		{
			$modifiedObjects = array_slice($this->objects, $offset, $itemsPerPage);
			return $modifiedObjects;
		}
		throw new \InvalidArgumentException (
			'The ViewHelper "'.static::class.'" accepts as argument "QueryResultInterface", "\SplObjectStorage", "ObjectStorage" or an array given: '.get_class($this->objects), 1385547291
		);
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}