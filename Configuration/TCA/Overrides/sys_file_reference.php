<?php
call_user_func(function () {
	// TEXTMEDIA: Add various media format links to sys_file_reference
	$temporaryColumns = [
		'tx_maagitprovider_m4v' => [
			'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:alternative',
			'config' => [
				'type' => 'link',
				'size' => 20,
				'appearance' => [
					'browserTitle' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:m4v',
					'allowedExtensions' => ['m4v'],
					'allowedTypes' => ['file']
				]
			]
		],
		'tx_maagitprovider_ogv' => [
			'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:alternative',
			'config' => [
				'type' => 'link',
				'size' => 20,
				'appearance' => [
					'browserTitle' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:ogv',
					'allowedExtensions' => ['ogv'],
					'allowedTypes' => ['file']
				]
			]
		],
		'tx_maagitprovider_webm' => [
			'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:alternative',
			'config' => [
				'type' => 'link',
				'size' => 20,
				'appearance' => [
					'browserTitle' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:webm',
					'allowedExtensions' => ['webm'],
					'allowedTypes' => ['file']
				]
			]
		],
		'tx_maagitprovider_cssclass' => [
			'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:cssclass',
			'config' => [
				'type' => 'input'
			]
		],
        'tx_maagitprovider_controls' => [
            'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:controls',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
        ],
		'tx_maagitprovider_loop' => [
            'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:loop',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
        ],
        'tx_maagitprovider_muted' => [
            'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:muted',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
        ],
        'tx_maagitprovider_playsinline' => [
            'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:playsinline',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 0,
            ],
		],
        'tx_maagitprovider_html' => [
            'label' => 'LLL:EXT:maagitprovider/Resources/Private/Language/locallang.xlf:html',
	        'config' => [
	            'type' => 'text',
	            'cols' => 40,
	            'rows' => 10
	        ],
		]
	];
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
	        'sys_file_reference',
	        $temporaryColumns
	);

	// TEXTMEDIA: Add new columns to type of VIDEO on sys_file_reference
	$GLOBALS['TCA']['sys_file_reference']['palettes']['videoOverlayPalette']['showitem'] = '
		title,description,--linebreak--,tx_maagitprovider_m4v,tx_maagitprovider_ogv,--linebreak--,tx_maagitprovider_webm,tx_maagitprovider_cssclass,--linebreak--,tx_maagitprovider_controls,autoplay,--linebreak--,tx_maagitprovider_loop,tx_maagitprovider_muted,--linebreak--,tx_maagitprovider_playsinline,--linebreak--,tx_maagitprovider_html
		';
});
?>