<?php
namespace Maagit\Maagitprovider\FormValidators;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Form
	class:				CompareValidator

	description:		Compare content of various elements.

	created:			2022-08-12
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2022-08-12	Urs Maag		Initial version
						2022-09-17	Urs Maag		Remove "ObjectManager" and static
													call to translation service in
													method "validateForFormFramework"
						2022-10-06	Urs Maag		Remove constructor and change
													the $supportedOptions to array
													values

------------------------------------------------------------------------------------- */


class CompareValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     *
     * @var string
     */
	protected $withField = '1';

	/**
     *
     * @var boolean
     */
	protected $negate = false;

	/**
     *
     * @var array
     */
	protected $supportedOptions = [
		'withField' => ['', 'Fieldname of which comparison has to be', 'string'],
		'negate' => [false, 'Negate comparison result?', 'boolean']
	];


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the withField
     *
     * @param	string	$withField
     */
    public function setWithField($withField)
    {
        $this->withField = $withField;
    }

	/**
     * Returns the withField
     *
     * @return	string	$withField
     */
    public function getWithField()
    {
		return $this->withField;
    }

	/**
     * Sets the negate
     *
     * @param	boolean	$negate
     */
    public function setNegate($negate)
    {
        $this->negate = $negate;
    }

	/**
     * Returns the negate
     *
     * @return	boolean	$negate
     */
    public function getNegate()
    {
		return $this->negate;
    }


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * @param	mixed[]															$requestArguments			the request arguments
	 * @param	\TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface		$renderable					the renderables
	 * @param	\TYPO3\CMS\Form\Mvc\ProcessingRule								$processingRule				the processing rule
	 * @throws Exception
	 */
	public function validateForFormFramework($requestArguments, \TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface $renderable, \TYPO3\CMS\Form\Mvc\ProcessingRule $processingRule)
	{
		$this->withField = $this->options['withField'];
		$this->negate = $this->options['negate']??false;

		// get arguments to validate
		$current = $requestArguments[$renderable->getIdentifier()];
		$target = $requestArguments[$this->substituteField($this->withField)];

		// if validation failes, add error
		if (($this->negate && $current === $target) || (!$this->negate && $current !== $target))
		{
			$processingRule = $renderable->getRootForm()->getProcessingRule($renderable->getIdentifier());
			$translationService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Form\Service\TranslationService::class);
			$errorMessage = $translationService->translate('validation.error.1660296857', null, 'EXT:maagitprovider/Resources/Private/Language/locallang.xlf');
			$error = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Validation\Error::class, $errorMessage, 1660296857);
			$processingRule->getProcessingMessages()->addError($error);
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
	 * @param	mixed			$value				the value to validate
	 * @return	boolean								the validation result
	 */
	protected function isValid(mixed $value): void
	{
		/* do literally nothing, as the check is located in Maagit\Maagitprovider\FormHooks\FormHooks
		* and the Validator is here for configuration purposes only
		*
		*/
	}

	/**
	 * @param	string			$field				the field name within { }
	 * @return	string								the substituted field name
	 */
	protected function substituteField($field)
	{
		$field = str_replace('{', '', $field);
		$field = str_replace('}', '', $field);
		return $field;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}