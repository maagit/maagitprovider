<?php
namespace Maagit\Maagitprovider\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Model
	class:				Field

	description:		Model for the "field". Inherits the form data from a
						given form field.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


abstract class Field extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $id;

	/**
     * @var string
     */
    protected $name;

	/**
     * @var mixed
     */
    protected $value;

	/**
     * @var string
     */
    protected $type;


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the id
     *
     * @param	string	$id
     */
    public function setId($id)
    {
        // @extensionScannerIgnoreLine
		$this->id = $id;
    }

	/**
     * Returns the id
     *
     * @return	string	$id
     */
    public function getId()
    {
		// @extensionScannerIgnoreLine
		return $this->id;
    }
	
	/**
     * Sets the name
     *
     * @param	string	$name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
     * Returns the name
     *
     * @return	string	$name
     */
    public function getName()
    {
		return $this->name;
    }

	/**
     * Sets the value
     *
     * @param	mixed	$value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

	/**
     * Returns the value
     *
     * @return	mixed	$value
     */
    abstract public function getValue();

	/**
     * Sets the type
     *
     * @param	string	$type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

	/**
     * Returns the type
     *
     * @return	string	$type
     */
    public function getType()
    {
		return $this->type;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */

	
	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}