<?php
namespace Maagit\Maagitprovider\FormFinishers;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Form
	class:				CustomSaveFinisher

	description:		Finisher for forms.
						Save the submitted form data to the database.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class SaveFinisher extends \TYPO3\CMS\Form\Domain\Finishers\AbstractFinisher
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * Executes this finisher
     * @see AbstractFinisher::execute()
     *
     * @throws FinisherException
     */
    protected function executeInternal()
    {
		$formName = $this->finisherContext->getFormRuntime()->getFormDefinition()->getLabel();
		$data = array();
		foreach ($this->getFormValues() as $key => $value)
		{
			$element = $this->getElementByIdentifier($key);
			if ($element instanceof \TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface)
			{
				$label = $this->getElementLabel($element);
				$value = $this->getElementValue($element);
				$type = $element->getType();
				if (!empty($label))
				{
					$data[$key]['label'] = $label;
					$data[$key]['value'] = $value;
					$data[$key]['type'] = $type;
				}
			}
		}
		$this->writeToDatabase($formName, $data);
    }

    /**
     * Write data to database
     *
     * @param	string			$formName			the form name
	 * @param	array			$data				the key/value pairs
	 * @return	void
     */
    protected function writeToDatabase(string $formName, array $data)
    {
        $table = 'tx_maagitprovider_domain_model_formdata';
		$databaseConnection = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool')->getConnectionForTable($table);
		$order = 0;
		$formid = uniqid('', true);

		foreach ($data as $key => $value)
		{
			$order += 10;
			$db['pid'] = $this->parseOption('pid');
			$db['formId'] = $formid;
			$db['formName'] = $formName;
			$db['fieldId'] = $key;
			$db['fieldName'] = $value['label'];
			$db['fieldValue'] = $value['value'];
			$db['fieldType'] = $value['type'];
			$db['fieldOrder'] = $order;
			$db['tstamp'] = time();
			$db['crdate'] = time();
			$databaseConnection->insert($table, $db);
		}
    }

    /**
     * Returns the label of a form field
     *
     * @return string
     */
    protected function getElementLabel(\TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface $element)
    {
        return $element->getLabel();
    }

	/**
     * Returns the value of a form field
     *
     * @return string
     */
    protected function getElementValue(\TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface $element)
    {
		$elementValue = $this->getFormValues()[$element->getIdentifier()];
		if ($elementValue instanceof FileReference)
		{
			$elementValue = $elementValue->getOriginalResource()->getCombinedIdentifier();
		}
		elseif (is_array($elementValue))
		{
			$elementValue = implode(',', $elementValue);
		}
		elseif ($elementValue instanceof \DateTimeInterface)
		{
			$format = 'U';
			$elementValue = $elementValue->format($format);
		}
        return $elementValue;
    }

    /**
     * Returns the values of the submitted form
     *
     * @return array
     */
    protected function getFormValues(): array
    {
        return $this->finisherContext->getFormValues();
    }

    /**
     * Returns a form element object for a given identifier.
     *
     * @param string $elementIdentifier
     * @return FormElementInterface|null
     */
    protected function getElementByIdentifier(string $elementIdentifier)
    {
        return $this
            ->finisherContext
            ->getFormRuntime()
            ->getFormDefinition()
            ->getElementByIdentifier($elementIdentifier);
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}