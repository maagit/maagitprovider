<?php
	return [
		'module-formdata' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			'source' => 'EXT:form/Resources/Public/Icons/Extension.svg',
		],
		'extensions-maagitprovider_content' => [
			'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
			'source' => 'EXT:maagitprovider/Resources/Public/Icons/tt_content_ts.png'
		]
	];
?>