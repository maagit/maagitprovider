<?php
namespace Maagit\Maagitprovider\Scss;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			scss
	class:				RenderPreProcessorHook

	description:		Generate css files from scss definitions.
						Uses the scssphp library.

	created:			2020-08-03
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-08-03	Urs Maag		Initial version
						2021-12-25	Urs Maag		PHP 8, non existing array keys
						2021-12-28	Urs Maag		Bugfix caching mechanism

------------------------------------------------------------------------------------- */


class RenderPreProcessorHook
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var array
     */
	private static $visitedFiles = [];

    /**
     * @var array
     */
    private $variables = [];

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    private $contentObjectRenderer;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
	 * Main hook function
	 *
	 * @param	array												$params 				Array of CSS/javascript and other files
	 * @param	\TYPO3\CMS\Core\Page\PageRendererPageRenderer		$pagerenderer			Pagerenderer object
	 * @return	void
	 */
	public function renderPreProcessorProc(&$params, \TYPO3\CMS\Core\Page\PageRenderer $pagerenderer)
	{
		// exit, if no css Files are defined
		if (!\is_array($params['cssFiles'])) {return;}

		// initialization
		$defaultOutputDir = 'typo3temp/assets/css/';
		$sitePath = \TYPO3\CMS\Core\Core\Environment::getPublicPath().'/';
		$request = $GLOBALS['TYPO3_REQUEST'];

// check of FE or BE
if (empty($request)) {return;}
$applicationType = $request->getAttribute('applicationType');
if ($applicationType != 1) {return;}

		// get typoscript settings
//		$setup = @$GLOBALS['TSFE']->tmpl->setup;
$setup = $request->getAttribute('frontend.typoscript')->getSetupArray();
		if (\is_array($setup))
		{
			if (isset($setup['plugin.']['tx_maagitprovider.']['variables.']) && \is_array($setup['plugin.']['tx_maagitprovider.']['variables.']))
			{
				$variables = $setup['plugin.']['tx_maagitprovider.']['variables.'];
				$parsedTypoScriptVariables = [];
				foreach ($variables as $variable => $key) {
					if (array_key_exists($variable.'.', $variables))
					{
						if ($this->contentObjectRenderer === null)
						{
							$this->contentObjectRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\ContentObject\\ContentObjectRenderer');
						}
						$content = $this->contentObjectRenderer->cObjGetSingle($variables[$variable], $variables[$variable . '.']);
						$parsedTypoScriptVariables[$variable] = $content;
					}
					elseif (substr($variable, -1) !== '.')
					{
						$parsedTypoScriptVariables[$variable] = $key;
					}
				}
				$this->variables = $parsedTypoScriptVariables;
			}
		}

		// get variables hash and create sanitizer object
		$variablesHash = \count($this->variables) > 0 ? hash('md5', implode(',', $this->variables)) : null;
        $filePathSanitizer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Resource\\FilePathSanitizer');

        // we need to rebuild the CSS array to keep order of CSS files
		$cssFiles = [];
		foreach ($params['cssFiles'] as $file => $conf)
		{
			$pathInfo = pathinfo($conf['file']);
			if (!isset($pathInfo['extension'])) {$pathInfo['extension'] = '';}
			if ($pathInfo['extension'] !== 'scss')
			{
				$cssFiles[$file] = $conf;
				continue;
			}
			$outputDir = $defaultOutputDir;
			$inlineOutput = false;
			$filename = $pathInfo['filename'];
			$formatter = null;
			$showLineNumber = false;
			$useSourceMap = false;
			$outputFile = null;

			// search settings for scss file
			if (\is_array($setup['page.']['includeCSS.']))
			{
				foreach ($setup['page.']['includeCSS.'] as $key => $subconf)
				{
					if (\is_string($setup['page.']['includeCSS.'][$key]) && $filePathSanitizer->sanitize($setup['page.']['includeCSS.'][$key]) === $file)
					{
						$outputDir = isset($setup['page.']['includeCSS.'][$key . '.']['outputdir']) ? trim($setup['page.']['includeCSS.'][$key . '.']['outputdir']) : $outputDir;
						$outputFile = isset($setup['page.']['includeCSS.'][$key . '.']['outputfile']) ? trim($setup['page.']['includeCSS.'][$key . '.']['outputfile']) : null;
						$formatter = isset($setup['page.']['includeCSS.'][$key . '.']['formatter']) ? trim($setup['page.']['includeCSS.'][$key . '.']['formatter']) : null;
						$showLineNumber = false;
						if (isset($setup['page.']['includeCSS.'][$key . '.']['linenumber']))
						{
							if ($setup['page.']['includeCSS.'][$key . '.']['linenumber'] === 'true' || (int)$setup['page.']['includeCSS.'][$key . '.']['linenumber'] === 1)
							{
								$showLineNumber = true;
							}
							$useSourceMap = isset($setup['page.']['includeCSS.'][$key . '.']['sourceMap']) ? true : false;
							if (isset($setup['page.']['includeCSS.'][$key . '.']['inlineOutput']))
							{
								$inlineOutput = (bool)trim($setup['page.']['includeCSS.'][$key . '.']['inlineOutput']);
							}
						}
					}
				}
			}

			// get output file
			if ($outputFile !== null)
			{
				$outputDir = \dirname($outputFile);
				$filename = basename($outputFile);
			}
			$outputDir = (substr($outputDir, -1) === '/') ? $outputDir : $outputDir . '/';
			if (!strcmp(substr($outputDir, 0, 4), 'EXT:'))
			{
				[$extKey, $script] = explode('/', substr($outputDir, 4), 2);
				if ($extKey && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded($extKey))
				{
					$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extKey);
					$outputDir = substr($extPath, \strlen($sitePath)) . $script;
				}
			}

			// create filename - hash is important due to the possible conflicts with same filename in different folders
			$scssFilename = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($conf['file']);
            \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir_deep($sitePath . $outputDir);
			if ($outputFile === null)
			{
				$cssRelativeFilename = $outputDir.$filename.(($outputDir === $defaultOutputDir) ? '_'.hash('sha1', $file) : (\count($this->variables) > 0 ? '_'.$variablesHash : '')).((substr($filename,-4) === '.css') ? '' : '.css');
			}
			else
			{
				$cssRelativeFilename = $outputDir.$filename.((substr($filename,-4) === '.css') ? '' : '.css');
			}
			$cssFilename = $sitePath.$cssRelativeFilename;

            // cache mechanism
			$cache = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Cache\\CacheManager')->getCache('maagitprovider_scss');
			$cacheKey = hash('sha1', $cssRelativeFilename);
			$contentHash = $this->calculateContentHash($scssFilename, implode(',', $this->variables));
			if ($showLineNumber)
			{
				$contentHash .= 'l1';
			}
			if ($useSourceMap)
			{
				$contentHash .= 'sm';
			}
			$contentHash .= $formatter;
			$contentHashCache = '';
			if ($cache->has($cacheKey))
			{
				$contentHashCache = $cache->get($cacheKey);
			}
			$css = '';
			try
			{
				if ($contentHashCache === '' || $contentHashCache !== $contentHash)
				{
					$css = $this->compileScss($scssFilename, $cssFilename, $this->variables, $showLineNumber, $formatter, $cssRelativeFilename, $useSourceMap);
					$cache->set($cacheKey, $contentHash, ['scss'], 0);
				}
			}
			catch (\Exception $ex)
			{
				\TYPO3\CMS\Core\Utility\DebugUtility::debug($ex->getMessage());
			}

            // write output
			if ($inlineOutput)
			{
				unset($cssFiles[$cssRelativeFilename]);
				if ($css === '') {
					$css = file_get_contents($cssFilename);
				}
				$params['cssInline'][$cssRelativeFilename] = [
					'code' => $css,
				];
			}
			else
			{
				$cssFiles[$cssRelativeFilename] = $params['cssFiles'][$file];
				$cssFiles[$cssRelativeFilename]['file'] = $cssRelativeFilename;
			}
		}
		$params['cssFiles'] = $cssFiles;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
	 * Compiling Scss with scss
	 *
	 * @param	string				$scssFilename				Existing scss file absolute path
	 * @param	string				$cssFilename				File to be written with compiled CSS
	 * @param	array				$vars						Variables to compile
	 * @param	boolean				$showLineNumber				Show line numbers
	 * @param	string				$formatter					formatter name
	 * @param	string 				$cssRelativeFilename		relative css filename
	 * @param	boolean				$useSourceMap				Use SourceMap
	 * @return	string											the compiled css code
	 */
	protected function compileScss($scssFilename, $cssFilename, $vars = [], $showLineNumber = false, $formatter = null, $cssRelativeFilename = null, $useSourceMap = false): string
	{
        $sitePath = \TYPO3\CMS\Core\Core\Environment::getPublicPath().'/';
		if (!class_exists(\ScssPhp\ScssPhp\Version::class, false))
		{
			$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('maagitprovider');
			require_once $extPath.'Resources/Private/Libraries/scssphp/scss.inc.php';
		}

		$cacheOptions = [
			'cacheDir' => $sitePath.'typo3temp/assets/css/cache/',
			'prefix' => md5($cssFilename),
		];
		$parser = new \ScssPhp\ScssPhp\Compiler($cacheOptions);
		if (file_exists($scssFilename))
		{
			$parser->setVariables($vars);
			if ($showLineNumber)
			{
				$parser->setLineNumberStyle(\ScssPhp\ScssPhp\Compiler::LINE_COMMENTS);
			}
			if ($formatter !== null)
			{
				$parser->setFormatter($formatter);
			}
            if ($useSourceMap)
			{
				$parser->setSourceMap(\ScssPhp\ScssPhp\Compiler::SOURCE_MAP_INLINE);
				$parser->setSourceMapOptions([
					'sourceMapWriteTo' => $cssFilename.'.map',
					'sourceMapURL' => $cssRelativeFilename.'.map',
					'sourceMapBasepath' => $sitePath,
					'sourceMapRootpath' => '/',
				]);
			}
			$css = $parser->compile('@import "'.$scssFilename.'";');
			\TYPO3\CMS\Core\Utility\GeneralUtility::writeFile($cssFilename, $css);
			return $css;
		}
		return '';
	}
	
	/**
	 * Calculating content hash to detect changes
	 *
	 * @param	string					$scssFilename			Existing scss file absolute path
	 * @param	string					$vars
	 * @return	string
	 */
	protected function calculateContentHash($scssFilename, $vars = ''): string
	{
		if (\in_array($scssFilename, self::$visitedFiles, true))
		{
			return '';
		}
		if (empty(@file_get_contents($scssFilename)))
		{
			return '';
		}

		self::$visitedFiles[] = $scssFilename;
		$content = file_get_contents($scssFilename);
		$pathinfo = pathinfo($scssFilename);
		$hash = hash('sha1', $content);
		if ($vars !== '')
		{
			$hash = hash('sha1', $hash.$vars);
		}

		preg_match_all('/@import "([^"]*)"/', $content, $imports);
		foreach ($imports[1] as $import)
		{
			$hashImport = '';
			if (file_exists($pathinfo['dirname'].'/'.$import.'.scss'))
			{
				$hashImport = $this->calculateContentHash($pathinfo['dirname'].'/'.$import.'.scss');
			}
			else
			{
				$parts = explode('/', $import);
				$filename = '_'.array_pop($parts);
				$parts[] = $filename;
				if (file_exists($pathinfo['dirname'].'/'.implode('/', $parts).'.scss'))
				{
					$hashImport = $this->calculateContentHash($pathinfo['dirname'].'/'.implode('/', $parts).'.scss');
				}
			}
			if ($hashImport !== '')
			{
				$hash = hash('sha1', $hash.$hashImport);
			}
		}
		return $hash;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}