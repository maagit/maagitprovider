CREATE TABLE tx_maagitprovider_domain_model_recordlist (
    fieldValue varchar(255) DEFAULT '' NOT NULL,
    fieldText varchar(255) DEFAULT '' NOT NULL,
    fieldAdd1 varchar(255) DEFAULT '',
    fieldAdd2 varchar(255) DEFAULT '',
    fieldAdd3 varchar(255) DEFAULT '',
	fieldAdd4 varchar(255) DEFAULT '',
	fieldAdd5 varchar(255) DEFAULT ''
);

CREATE TABLE tx_maagitprovider_domain_model_formdata (
   uid int(11) unsigned DEFAULT 0 NOT NULL auto_increment,
   pid int(11) DEFAULT 0 NOT NULL,
   formId varchar(255) DEFAULT '' NOT NULL,
   formName varchar(255),
   fieldId varchar(255),
   fieldName varchar(255),
   fieldValue varchar(255),
   fieldType varchar(255),
   fieldOrder int(11) unsigned DEFAULT 0 NOT NULL,
   tstamp int(11) unsigned DEFAULT 0 NOT NULL,
   crdate int(11) unsigned DEFAULT 0 NOT NULL,
   PRIMARY KEY (uid),
   KEY parent (pid),
);

CREATE TABLE sys_file_reference (
	tx_maagitprovider_m4v tinytext,
	tx_maagitprovider_ogv tinytext,
	tx_maagitprovider_webm tinytext,
	tx_maagitprovider_cssclass tinytext,
	tx_maagitprovider_controls tinyint(1),
	tx_maagitprovider_loop tinyint(1),
	tx_maagitprovider_muted tinyint(1),
	tx_maagitprovider_playsinline tinyint(1),
	tx_maagitprovider_html text
);