<?php
return [
	'ctrl' => [
		'title' => 'Record List',
		'label' => 'fieldText',
		'sortby' => 'sorting',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'delete' => 'deleted',
		'groupName' => 'testest',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
			'fe_group' => 'fe_group'
		],
        'iconfile' => 'EXT:maagitprovider/Resources/Public/Icons/recordlist.svg',
	 ],
	 'interface' => [
	 
	 ],
     'types' => [
		 '1' => [
			 'showitem' => '
				--div--;Recordlist entry,
			 	fieldValue,
				fieldText,
				fieldAdd1,
				fieldAdd2,
				fieldAdd3,
				fieldAdd4,
				fieldAdd5,
				--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
                hidden,
                --palette--;;access,
			'
		]
	 ],
	 'palettes' => [
         'access' => [
             'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access',
             'showitem' => '
                 starttime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:starttime_formlabel,
                 endtime;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:endtime_formlabel,
                 --linebreak--,
				 fe_group;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:fe_group_formlabel,
             '
		 ]
	 ],
	 'columns' => [
		 'hidden' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
			 'config' => [
				 'type' => 'check',
				 'renderType' => 'checkboxToggle',
				 'items' => [
					 [
						 'label' => 'Visible',
						 'labelChecked' => 'Enabled',
						 'labelUnchecked' => 'Disabled',
						 'invertStateDisplay' => true
					]
				]
			 ]
		 ],
		 'starttime' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
			 'config' => [
				 'type' => 'datetime',
				 'default' => 0
			 ],
			 'l10n_mode' => 'exclude',
			 'l10n_display' => 'defaultAsReadonly'
		 ],
		 'endtime' => [
			 'exclude' => true,
			 'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
			 'config' => [
				 'type' => 'datetime',
				 'default' => 0
			 ],
			 'l10n_mode' => 'exclude',
			 'l10n_display' => 'defaultAsReadonly'
		 ],
		 'fieldValue' => [
			 'label' => 'Value',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255,
				 'required' => true
			 ]
		 ],
		 'fieldText' => [
			 'label' => 'Text',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255,
				 'required' => true
			 ]
		 ],
		 'fieldAdd1' => [
			 'label' => 'Additional 1',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ],
		 'fieldAdd2' => [
			 'label' => 'Additional 2',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ],
		 'fieldAdd3' => [
			 'label' => 'Additional 3',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ],
		 'fieldAdd4' => [
			 'label' => 'Additional 4',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ],
		 'fieldAdd5' => [
			 'label' => 'Additional 5',
			 'config' => [
				 'type' => 'input',
				 'size' => 50,
				 'max' => 255
			 ]
		 ]
	 ]
];
