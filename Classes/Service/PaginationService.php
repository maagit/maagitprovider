<?php
namespace Maagit\Maagitprovider\Service;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Service
	class:				PaginationService

	description:		Methods for paginating objects.

	created:			2020-12-25
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-12-25	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class PaginationService
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
    /**
     * @var TYPO3\CMS\Extbase\Pagination\QueryResultPaginator|TYPO3\CMS\Core\Pagination\ArrayPaginator
     */
	protected $paginator;

	/**
     * @var int
     */
	protected $currentPage = 1;

	/**
     * @var int
     */
	protected $itemsPerPage = 10;

    /**
     * @var int
     */
	protected $numberOfObjects = 0;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Initialize the object
     *
     */
	public function __construct($objects, int $currentPage=0, int $itemsPerPage=0)
	{
		// initialize member variables
		$this->currentPage = ($currentPage > 0) ? $currentPage : $this->currentPage;
		$this->itemsPerPage = ($itemsPerPage > 0) ? $itemsPerPage : $this->itemsPerPage;
		$this->numberOfObjects = count($objects);

		// get paginator object
		if ($objects instanceof \TYPO3\CMS\Extbase\Persistence\QueryResultInterface)
		{
			$this->paginator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Pagination\\QueryResultPaginator', $objects, $this->currentPage, $this->itemsPerPage);
		}
		else
		{
			$this->paginator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Pagination\\ArrayPaginator', $objects, $this->currentPage, $this->itemsPerPage);
		}
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * create pagination arguments for using in a view
	 *
	 * @return	array									the paginate arguments
     */
	public function getPaginationArguments()
	{
		$paginationArguments = $this->buildPagination();
		$paginationArguments['paginatedItems'] = $this->paginator->getPaginatedItems();
		return $paginationArguments;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * build pagination arguments, based on given member variables
     *
     * @return array
     */
    protected function buildPagination()
    {
		$endRecord = $this->paginator->getKeyOfFirstPaginatedItem() + $this->itemsPerPage;
		if ($endRecord > $this->numberOfObjects)
		{
			$endRecord = $this->numberOfObjects;
		}

		$pagination = array(
			'current' => $this->currentPage,
			'numberOfPages' => $this->paginator->getNumberOfPages(),
			'hasLessPages' => $this->currentPage > 1,
			'hasMorePages' => $this->currentPage < $this->paginator->getNumberOfPages(),
			'startRecord' => $this->paginator->getKeyOfFirstPaginatedItem() + 1,
			'endRecord' => $endRecord
		);
		if ($this->currentPage < $this->paginator->getNumberOfPages())
		{
			$pagination['nextPage'] = $this->currentPage + 1;
		}
		if ($this->currentPage > 1) {
            $pagination['previousPage'] = $this->currentPage - 1;
		}
		return $pagination;
    }


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}