<?php
defined('TYPO3') or die();

call_user_func(function () {
	// STRUCTURE: register "renderPreProcessor" hook for validating/creating necessary fileadmin structure
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess']['maagitprovider_structure'] = \Maagit\Maagitprovider\Structure\RunningStructure::class.'->validate';

	// FORM: register language file of extension tx_form for backend
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['locallangXMLOverride']['EXT:form/Resources/Private/Language/Database.xlf'][] = 'EXT:maagitprovider/Resources/Private/Language/Backend.xlf';

	// FORM: register tx_form Hooks "beforeFormSave" and "beforeFormDelete"
	//       (autogenerating typoscript for default values, e.g. to set values over url parameters)
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['beforeFormDelete'][1577272396] = \Maagit\Maagitprovider\FormHooks\FormHooks::class;
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['beforeFormSave'][1577272396] = \Maagit\Maagitprovider\FormHooks\FormHooks::class;

	// FORM: register tx_form Hook "afterSubmit"
	//       (loop and execute the "compare" validators)
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['afterSubmit'][1660392161] = \Maagit\Maagitprovider\FormHooks\FormHooks::class;

	// FORM: extend class "FormRuntime" to make a better honeypot element
	$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][TYPO3\CMS\Form\Domain\Runtime\FormRuntime::class] = [
	   'className' => Maagit\Maagitprovider\FormRuntime\FormRuntime::class
	];

	// TS CONTENT: configure plugin "typoscript as content element"
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'Maagitprovider',
		'Content',
		[\Maagit\Maagitprovider\Controller\ContentController::class => 'index'],
		[],
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
	);

	// SCSS: register "renderPreProcessor" hook for compiling scss files
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-preProcess']['maagitprovider'] = \Maagit\Maagitprovider\Scss\RenderPreProcessorHook::class.'->renderPreProcessorProc';

	// SCSS: register cache
	if (!isset($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['maagitprovider_scss'])) {
		$GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['maagitprovider_scss'] = [];
	}

	// RTE: register own ckeditor preset
	if (empty($GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['maagitprovider'])) {
		$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['maagitprovider'] = 'EXT:maagitprovider/Configuration/RTE/Default.yaml';
	}
});