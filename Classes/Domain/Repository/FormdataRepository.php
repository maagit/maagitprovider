<?php
namespace Maagit\Maagitprovider\Domain\Repository;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Repository
	class:				FormdataRepository

	description:		Repository for the form datas.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version
						2021-09-09	Urs Maag		Avoid parent constructor, which
													uses already depreceated class
													"ObjectManager"

------------------------------------------------------------------------------------- */


class FormdataRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var array
     */
    protected $forms;

	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
     * Avoid parent constructor (uses depreceated class "ObjectManager") until v12
     *
     * @param	-
	 * @return 	void
     */
	public function __construct()
	{
		
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Select form datas by given pid.
     *
     * @param	int		$pid    	the pid
	 * @return 	array				array of form datas
     */
	public function findByPid(int $pid)
	{
		// build query
		$connectionPool = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool');
		$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_maagitprovider_domain_model_formdata');
		$queryBuilder
			->select('tx_maagitprovider_domain_model_formdata.*')
			->from('tx_maagitprovider_domain_model_formdata')
			->where(
				$queryBuilder->expr()->eq(
					'tx_maagitprovider_domain_model_formdata.pid',
					$queryBuilder->createNamedParameter($pid, \TYPO3\CMS\Core\Database\Connection::PARAM_INT)
				)
			)
			->orderBy('tx_maagitprovider_domain_model_formdata.formId', 'asc')
			->addOrderBy('tx_maagitprovider_domain_model_formdata.fieldOrder', 'asc')
		;

		// execute query and fetch records
		$records = $queryBuilder->executeQuery()->fetchAllAssociative();
		$this->convertToFormdataRecords($records);

		// sort on crdate, descending
		$sortingService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Service\\SortingService');
		foreach ($this->forms as $form)
		{
			$form->setRequests($sortingService->sort($form->getRequests(), array('getCrdate' => 'desc')));
		}

		// return the forms
		return $this->forms;
	}
	
	/**
     * Delete form datas by given requestId.
     *
     * @param	string		$requestId    	the request id
	 * @return 	void
     */
	public function removeByRequestId(string $requestId)
	{
		$connectionPool = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Database\\ConnectionPool');
		$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_maagitprovider_domain_model_formdata');
		$queryBuilder->delete('tx_maagitprovider_domain_model_formdata')->where($queryBuilder->expr()->eq('formid', $queryBuilder->createNamedParameter($requestId)))->executeQuery();
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Convert database records to formdata record.
     *
     * @param	array		$dbRecords    	the database records
	 * @return 	array						array of form datas
     */
	protected function convertToFormdataRecords(array $dbRecords)
	{
		// initialization
		$this->forms = array();
		$previousFormId = 'INIT';
		$formName = '';

		// loop records and create forms with its data
		foreach ($dbRecords as $dbRecord)
		{
			// get|create form
			$formName = $dbRecord['formName'];
			$formId = $dbRecord['formId'];
			$form = $this->getForm($formName);

			// add field value to given form data request
			$form->addColumn($dbRecord['fieldName']);
			$form->getRequest($formId, $dbRecord['crdate'])->addField($dbRecord['fieldId'], $dbRecord['fieldName'], $dbRecord['fieldValue'], $dbRecord['fieldType']);
		}

		// return the global forms array
		return $this->forms;
	}

	/**
     * Get form object
     *
     * @param	string											$formName    	the form name
	 * @return 	\Maagit\Maagitprovider\Domain\Model\Form						the form object
     */
	protected function getForm(string $formName)
	{
		foreach ($this->forms as $form)
		{
			if ($form->getName() == $formName)
			{
				return $form;
			}
		}
		$newForm = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\Form');
		$newForm->setName($formName);
		$this->forms[] = $newForm;
		return $newForm;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}