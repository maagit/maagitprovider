<?php
/**
 * SCSSPHP
 *
 * @copyright 2012-2020 Leaf Corcoran
 *
 * @license http://opensource.org/licenses/MIT MIT
 *
 * @link http://scssphp.github.io/scssphp
 */

namespace ScssPhp\ScssPhp;

/**
 * Block
 *
 * @author Anthon Pang <anthon.pang@gmail.com>
 */
class Block
{
    /**
     * @var string
     */
    public $type;
	
    /**
     * @var string
     */
    public $var;
	
    /**
     * @var string
     */
    public $name;
	
    /**
     * @var string
     */
    public $list;
	
    /**
     * @var string
     */
    public $value;

    /**
     * @var \ScssPhp\ScssPhp\Block
     */
    public $parent;
	
    /**
     * @var \ScssPhp\ScssPhp\Compiler\Environment
     */
    public $parentEnv;
	
    /**
     * @var \ScssPhp\ScssPhp\Compiler\Environment
     */
    public $scope;

    /**
     * @var string
     */
    public $sourceName;

    /**
     * @var integer
     */
    public $sourceIndex;
	
    /**
     * @var integer
     */
    public $start;
	
    /**
     * @var integer
     */
    public $until;
	
    /**
     * @var integer
     */
    public $end;

    /**
     * @var integer
     */
    public $sourceLine;

    /**
     * @var integer
     */
    public $sourceColumn;
	
    /**
     * @var integer
     */
    public $with;
	
    /**
     * @var boolean
     */
    public $dontAppend;

    /**
     * @var array
     */
    public $selector;

	/**
     * @var array
     */
    public $selectors;
	
    /**
     * @var array
     */
    public $args;
	
    /**
     * @var array
     */
    public $vars;
	
    /**
     * @var array
     */
    public $cond;
	
    /**
     * @var array
     */
    public $cases;

    /**
     * @var array
     */
    public $comments;

    /**
     * @var array
     */
    public $children;
	
    /**
     * @var array
     */
    public $child;
	
    /**
     * @var array
     */
    public $queryList;

    /**
     * @var \ScssPhp\ScssPhp\Block
     */
    public $selfParent;
}
