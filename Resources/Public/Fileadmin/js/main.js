function cycleImages() {
	var $active = $('.slider .active');
	var $next = ($active.next().not('.base').length > 0) ? $active.next().not('.base') : $('.slider img').not('.base').first();
	$next.css('z-index', 2);																//move the next image up the pile
	$active.fadeOut(1500, function() {														//fade out the top image
		$active.css('z-index', 1).show().removeClass('active');								//reset the z-index and unhide the image
		$next.css('z-index', 3).addClass('active');											//make the next image the top one
	});
}

function scrollParallax() {
	var $el = $('.parallax-bg');
	var scroll = $(document).scrollTop();
	$el.css({
		'background-position':'50% '+(-.3*scroll)+'px'
	});
}

$(document).ready(function() {
	// PARALLAX: initialize scroll effect
	$(window).on('scroll', scrollParallax);

	// SLIDER: duplicate first image and make first image active
	$('.slider img:first').clone().prependTo('.slider');
	$('.slider img:first').addClass('base');
	$('.slider img').eq(1).addClass('active');
	$('.slider img').show();

	// SLIDER: run interval
	setInterval('cycleImages()', 7000);
});