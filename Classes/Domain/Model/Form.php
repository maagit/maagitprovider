<?php
namespace Maagit\Maagitprovider\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Model
	class:				Form

	description:		Model for the "form".

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var string
     */
    protected $name;

	/**
     * @var string
     */
    protected $id;

	/**
     * @var array
     */
	protected $columns = array();

	/**
     * @var array
     */
	protected $requests = array();

	/**
     * @var array
     */
	protected $pagination = array();


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the  name
     *
     * @param	string	$name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
     * Returns the name
     *
     * @return	string	$name
     */
    public function getName()
    {
		return $this->name;
    }

	/**
     * Sets the columns
     *
     * @param	array	$columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

	/**
     * Returns the columns
     *
     * @return	array	$columns
     */
    public function getColumns()
    {
		return $this->columns;
    }

	/**
     * Sets the requests
     *
     * @param	array	$requests
     */
    public function setRequests($requests)
    {
        $this->requests = $requests;
    }

	/**
     * Returns the request
     *
     * @return	array	$requests
     */
    public function getRequests()
    {
		return $this->requests;
    }

	/**
     * Returns the id for using in html elements
     *
     * @return	string	$id
     */
    public function getId()
    {
		return urlencode($this->name);
    }

	/**
     * Sets the pagination
     *
     * @param	array	$pagination
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

	/**
     * Returns the pagination
     *
     * @return	array	$pagination
     */
    public function getPagination()
    {
		return $this->pagination;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Get request object
     *
     * @param	string											$formId	    	the form id
	 * @param	int												$crdate	    	the crdate of form request
	 * @return 	\Maagit\Maagitprovider\Domain\Model\Requet						the request object
     */
	public function getRequest(string $formId, int $crdate)
	{
		foreach ($this->requests as $request)
		{
			if ($request->getId() == $formId)
			{
				return $request;
			}
		}
		$newRequest = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\Request', $this);
		$newRequest->setId($formId);
		$newRequest->setCrdate($crdate);
		$this->requests[] = $newRequest;
		return $newRequest;
	}

	/**
     * Add a column
     *
     * @param	string											$fieldName    	the field name
	 * @return 	void
     */
	public function addColumn(string $name)
	{
		$found = false;
		foreach ($this->columns as $key => $col)
		{
			if ($col == $name)
			{
				$found = true;
				break;
			}
		}
		if (!$found)
		{
			$this->columns[] = $name;
		}
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}