<?php
namespace Maagit\Maagitprovider\Domain\Model;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Model
	class:				Request

	description:		Model for the "request". Inherits the form data from a
						submitted form.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version

------------------------------------------------------------------------------------- */


class Request extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \Maagit\Maagitprovider\Domain\Model\Form
     */
	protected $form;

	/**
     * @var string
     */
    protected $id;

	/**
     * @var int
     */
    protected $crdate;

	/**
     * @var array
     */
	protected $fields;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	public function __construct(\Maagit\Maagitprovider\Domain\Model\Form $form)
	{
		$this->form = $form;
	}


	/* ======================================================================================= */
	/* P R O P E R T I E S                                                                     */
	/* ======================================================================================= */
	/**
     * Sets the id
     *
     * @param	string	$id
     */
    public function setId($id)
    {
        // @extensionScannerIgnoreLine
		$this->id = $id;
    }

	/**
     * Returns the id
     *
     * @return	string	$id
     */
    public function getId()
    {
		// @extensionScannerIgnoreLine
		return $this->id;
    }

	/**
     * Sets the crdate
     *
     * @param	int	$crdate
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }

	/**
     * Returns the crdate
     *
     * @return	int	$crdate
     */
    public function getCrdate()
    {
		return $this->crdate;
	}

	/**
     * Sets the fields
     *
     * @param	array	$fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

	/**
     * Returns the fields
     *
     * @return	array	$fields
     */
    public function getFields()
    {
		return $this->fields;
    }


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
	/**
     * Add a field with its value
     *
     * @param	string											$fieldId    	the field id
	 * @param	string											$fieldName    	the field name
	 * @param	mixed											$fieldValue   	the field value
	 * @param	string											$fieldType    	the field type
	 * @return 	void
     */
	public function addField(string $fieldId, string $fieldName, $fieldValue, string $fieldType)
	{
		$newField = $this->getFieldObject($fieldType, $fieldValue);
		$newField->setId($fieldName);
		$newField->setName($fieldName);
		$newField->setValue($fieldValue);
		$newField->setType($fieldType);
		$this->fields[] = $newField;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
	/**
     * Get field class, based on given value
     *
	 * @param	string											$type	    		the type
	 * @param	mixed											$value			   	the value
	 * @return 	\Maagit\Maagitprovider\Domain\Model\Field							the field class
     */
	public function getFieldObject(string $type, $value)
	{
		if ($type == 'Textarea')
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\FieldText');
		}
		elseif ($type == 'Checkbox')
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\FieldBoolean');
		}
		elseif ($type == 'Date' && !empty($value))
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\FieldDateTime');
		}
		elseif ($type == 'DatePicker' && !empty($value))
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\FieldDateTime');
		}
		else
		{
			$object = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Model\\FieldString');	
		}
		return $object;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}