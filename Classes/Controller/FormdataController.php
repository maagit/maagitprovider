<?php
namespace Maagit\Maagitprovider\Controller;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Controller
	class:				FormdataController

	description:		Backend module for displaying datas from submitted forms.

	created:			2020-07-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-07-29	Urs Maag		Initial version
						2020-12-25	Urs Maag		Pagination added
						2021-09-09	Urs Maag		ObjectManager removed
						2021-12-24	Urs Maag		PHP 8, fix non existing array keys

------------------------------------------------------------------------------------- */


class FormdataController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	/**
     * @var \TYPO3\CMS\Backend\Template\ModuleTemplateFactory
     */
	protected $moduleTemplateFactory;

	/**
     * @var int
     */
    protected $id;

	/**
     * @var string
     */
    protected $requestId;

	/**
     * @var int
     */
    protected $currentPage;
	
	/**
     * @var string
     */
    protected $key;


	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */
	/**
	 * Module constructor
	 *
	 * @return void
	 */
	public function __construct(\TYPO3\CMS\Backend\Template\ModuleTemplateFactory $moduleTemplateFactory)
	{
		$this->moduleTemplateFactory = $moduleTemplateFactory;
	}

	/**
	 * Action initializer
	 *
	 * @return void
	 */
	protected function initializeAction(): void
	{
		// @extensionScannerIgnoreLine
		$this->id = $this->request->getParsedBody()['id'] ?? $this->request->getQueryParams()['id'] ?? 0;
		$this->requestId = $this->request->getParsedBody()['requestId'] ?? $this->request->getQueryParams()['requestId'] ?? '';
		$this->currentPage = $this->request->getParsedBody()['currentPage'] ?? $this->request->getQueryParams()['currentPage'] ?? 1;
		$this->currentPage = (int)$this->currentPage;
		$this->key = $this->request->getParsedBody()['key'] ?? $this->request->getQueryParams()['key'] ?? '';
	}


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
	 * show form datas
     */
    public function formdataAction()
    {
		$formdataRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Repository\\FormdataRepository');
		// @extensionScannerIgnoreLine
		$data = $formdataRepository->findByPid((int)$this->id);
		// @extensionScannerIgnoreLine
		if (empty($this->id) || $this->id == 0)
		{
			$this->addFlashMessage(
			   \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('LLL:EXT:maagitprovider/Resources/Private/Language/locallang_mod.xlf:no.page.selected', 'maagitprovider'),
			   '',
			   \TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::INFO,
			   false
			);
		}
		// @extensionScannerIgnoreLine
		if ($this->id > 0 && empty($data))
		{
			$this->addFlashMessage(
				\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('LLL:EXT:maagitprovider/Resources/Private/Language/locallang_mod.xlf:no.data.found', 'maagitprovider'),
				'',
				\TYPO3\CMS\Core\Type\ContextualFeedbackSeverity::INFO,
				false
			);
		}
		foreach ($data as $form)
		{
			$page = ($form->getId() == $this->key) ? $this->currentPage : 1;
			$page = (!empty($page)) ? $page : 1;
			$paginationService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Service\\PaginationService', $form->getRequests(), $page);
			$form->setPagination($paginationService->getPaginationArguments());
		}
		$moduleTemplate = $this->moduleTemplateFactory->create($this->request);
		$moduleTemplate->setTitle('Forms Data');
		$moduleTemplate->assign('data', $data);
		// @extensionScannerIgnoreLine
		$moduleTemplate->assign('settings', array('pid' => $this->id));
		return $moduleTemplate->renderResponse('/Formdata');
    }

    /**
	 * delete form data record
     */
    public function deleteAction(): \Psr\Http\Message\ResponseInterface
    {
		if (!empty($this->requestId) && $this->requestId <> 0)
		{
			$formdataRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('Maagit\\Maagitprovider\\Domain\\Repository\\FormdataRepository');
			$formdataRepository->removeByRequestId($this->requestId);	
		}
		return new \TYPO3\CMS\Extbase\Http\ForwardResponse('formdata');
    }


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
}