<?php
namespace Maagit\Maagitprovider\FormHooks;


/*  =======================================================================================
 *  Copyright notice
 *
 *  2020-2020 Urs Maag <urs@maagit.ch>, maagIT Matzingen, CH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public $License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public $License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public $License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
======================================================================================== */


/*  ------------------------------------------------------------------------------------
	Vendor:				maagIT
	Extension:			Maagitprovider
	Package:			Form
	class:				FormHooks

	description:		Add form hooks to save or delete default typoscript code for
						given form definition. Add form hooks to validate user-defined
						validators

	created:			2020-06-29
	author:				Urs Maag (info@maagit.ch)

	changes:			YYYY-MM-DD	author			change description
						----------	--------------	------------------------------------
						2020-06-29	Urs Maag		Initial version
						2020-08-14	Urs Maag		add "prototype" field
						2022-08-13	Urs Maag		added "afterSubmit" hook
						2023-05-07	Urs Maag		Fix getting correct fileadmin path

------------------------------------------------------------------------------------- */


class FormHooks
{
	/* ======================================================================================= */
	/* M E M B E R   V A R I A B L E S                                                         */
	/* ======================================================================================= */
	
	
	/* ======================================================================================= */
	/* C O N S T R U C T O R S                                                                 */
	/* ======================================================================================= */


	/* ======================================================================================= */
	/* P U B L I C   M E T H O D S                                                             */
	/* ======================================================================================= */
    /**
     * delete typoscript code file
     *
     * @param	string			$formPersistenceIdentifier		the form identifier
     * @return	void
     */
	public function beforeFormDelete(string $formPersistenceIdentifier)
	{
		try {
			unlink($this->getTyposcriptFilename($formPersistenceIdentifier));	
		}
		catch (\Exception $e)
		{
		  
		}
	}
	
    /**
     * create and save typoscript code file
	 * set field "prototype" with given custom prototype value
     *
     * @param	string			$formPersistenceIdentifier		the form identifier
	 * @param	array			$formDefinition					the form definition
     * @return	array											the form definition
     */
	public function beforeFormSave(string $formPersistenceIdentifier, array $formDefinition): array
	{
		@file_put_contents($this->getTyposcriptFilename($formPersistenceIdentifier), $this->getFormTyposcriptContent($formDefinition));
		if (!empty($formDefinition['renderingOptions']['customPrototype']))
		{
			$formDefinition['prototypeName'] = $formDefinition['renderingOptions']['customPrototype'];
		}
		return $formDefinition;
	}

    /**
     * loop elements and validate existing "compare" validators
     *
     * @param	\TYPO3\CMS\Form\Domain\Runtime\FormRuntime							$formRuntime		the form runtime
	 * @param	\TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface			$renderable			the renderable
	 * @param	mixed																$elementValue		the element value
	 * @param	array																$requestArguments	the request arguments
     * @return	mixed																					the element value
     */
	public function afterSubmit(\TYPO3\CMS\Form\Domain\Runtime\FormRuntime $formRuntime, \TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface $renderable, $elementValue, array $requestArguments = [])
	{
		// validate user-defined "compare" validators
		$elementValue = $this->validateCompareValidators($formRuntime, $renderable, $elementValue, $requestArguments);

		// return element value
		return $elementValue;
	}


	/* ======================================================================================= */
	/* P R O T E C T E D   M E T H O D S                                                       */
	/* ======================================================================================= */
    /**
     * loop elements and validate existing "compare" validators
     *
     * @param	\TYPO3\CMS\Form\Domain\Runtime\FormRuntime							$formRuntime		the form runtime
	 * @param	\TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface			$renderable			the renderable
	 * @param	mixed																$elementValue		the element value
	 * @param	array																$requestArguments	the request arguments
     * @return	mixed																					the element value
     */
	protected function validateCompareValidators(\TYPO3\CMS\Form\Domain\Runtime\FormRuntime $formRuntime, \TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface $renderable, $elementValue, array $requestArguments = [])
	{
		$formDefinition = $formRuntime['formDefinition'];
		$processingRules = $formDefinition->getProcessingRules();
		foreach ($processingRules as $field => $processingRule)
		{
			$validators = $processingRule->getValidators();
			if ($field === $renderable->getIdentifier())
			{
				foreach ($validators as $validator) {
					if ($validator instanceof \Maagit\Maagitprovider\FormValidators\CompareValidator)
					{
						$validator->validateForFormFramework($requestArguments, $renderable, $processingRule);
					}
				}
			}
		}
		return $elementValue;
	}


	/* ======================================================================================= */
	/* P R I V A T E   M E T H O D S                                                           */
	/* ======================================================================================= */
	/**
     * create and save typoscript code file
     *
     * @param	string			$formPersistenceIdentifier		the form identifier
     * @return	string											the filename of typoscript file
     */
	private function getTyposcriptFilename(string $formPersistenceIdentifier)
	{
		$resourceFactory = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\ResourceFactory');
		$file = $resourceFactory->getFileObjectFromCombinedIdentifier($formPersistenceIdentifier);
		$pathAndFilename = $file->getPublicUrl();
		$typoscriptFile = str_replace('.yaml', '.typoscript', $pathAndFilename);
		$typoscriptFile = \TYPO3\CMS\Core\Core\Environment::getPublicPath().$typoscriptFile;
		return $typoscriptFile;
	}

    /**
     * create the typoscript content
     *
     * @param	array			$formDefinition			the form definition
     * @return	string									the typoscript file content
     */
	private function getFormTyposcriptContent($formDefinition)
	{
		// header
		$content = "plugin.tx_form {\n";
    	$content .= "\tsettings {\n";
        $content .= "\t\tformDefinitionOverrides {\n";
        $content .= "\t\t\t# identifier of form\n";
		$content .= "\t\t\t".$formDefinition['identifier']." {\n";
		$content .= "\t\t\t\trenderables {\n";

		// pages
		$pageCount = -1;
		foreach ($formDefinition['renderables'] as $page)
		{
			if ($page['type'] == 'Page')
			{
				$pageCount++;
			
				//header
				$content .= "\t\t\t\t\t# ".$page['identifier']."\n";
				$content .= "\t\t\t\t\t".$pageCount." {\n";
				$content .= "\t\t\t\t\t\trenderables {\n";
				
				if (isset($page['renderables']))
				{
					$elementCount = -1;
					foreach ($page['renderables'] as $element)
					{
						$elementCount++;
					
						//header
						$content .= "\t\t\t\t\t\t\t# ".$element['identifier']."\n";
						$content .= "\t\t\t\t\t\t\t".$elementCount." {\n";

						// defaultValue
						$content .= "\t\t\t\t\t\t\t\tdefaultValue = TEXT\n";
						$content .= "\t\t\t\t\t\t\t\tdefaultValue {\n";
						$content .= "\t\t\t\t\t\t\t\t\tstdWrap.data = GP:".$element['identifier']."\n";
						$content .= "\t\t\t\t\t\t\t\t\tstdWrap.insertData = 1\n";
						$content .= "\t\t\t\t\t\t\t\t}\n";

						// footer
						$content .= "\t\t\t\t\t\t\t}\n";
					}		
				}
				
				// footer
				$content .= "\t\t\t\t\t\t}\n";
				$content .= "\t\t\t\t\t}\n";
			}
		}

		// footer
		$content .= "\t\t\t\t}\n";
		$content .= "\t\t\t}\n";
		$content .= "\t\t}\n";
		$content .= "\t}\n";
		$content .= "}\n";
		return $content;
	}
}
